If not exists (SELECT * FROM sys.databases WHERE name = 'RSS_DataBase')
	BEGIN
		CREATE DATABASE RSS_DataBase;
		PRINT 'Database RSS_DataBase created successfully'
	END
Else
	BEGIN
		PRINT 'Database RSS_DataBase already created'
	END
IF exists (SELECT * FROM sys.databases WHERE name = 'RSS_DataBase')
	BEGIN
		USE RSS_DataBase
		IF not exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='Channel')
			BEGIN
				CREATE TABLE [dbo].[Channel] (
					[Id]          INT            IDENTITY (1, 1) NOT NULL,
					[name]        VARCHAR (100)  DEFAULT (' ') NULL,
					[title]       VARCHAR (500)  DEFAULT (' ') NULL,
					[description] VARCHAR (2000) DEFAULT (' ') NULL,
					[URL]         VARCHAR (500)  DEFAULT (' ') NULL,
					PRIMARY KEY CLUSTERED ([Id] ASC)
					)
				PRINT 'Table Channel created successfully'
			END
		ELSE
			BEGIN
				PRINT 'Table Channel already exists' 
			END
		IF not exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='News')
			BEGIN
				CREATE TABLE [dbo].[News] (
					[Id]          INT            IDENTITY (1, 1) NOT NULL,
					[title]       VARCHAR (500)  DEFAULT (' ') NULL,
					[description] VARCHAR (2000) DEFAULT (' ') NULL,
					[URL]         VARCHAR (500)  DEFAULT (' ') NULL,
					[Channel_Id]  INT            NOT NULL,
					PRIMARY KEY CLUSTERED ([Id] ASC),
					FOREIGN KEY ([Channel_Id]) REFERENCES [dbo].[Channel] ([Id])
					)
				PRINT 'Table News created successfully'
			END
		ELSE
			BEGIN
				PRINT 'Table News already exists' 
			END
	END
