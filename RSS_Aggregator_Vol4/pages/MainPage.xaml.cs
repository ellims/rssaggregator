﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RSS_Aggregator_Vol4.EllimsRssService;
using RSS_Aggregator_Vol4.pages;

using System.Windows.Navigation;
using System.ServiceModel;

namespace RSS_Aggregator_Vol4
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        { 
            InitializeComponent();
            SetUp();
        }
        /// <summary>
        /// Sets start options
        /// </summary>
        private void SetUp()
        {          
            
            RSSnewsDataGrid.Visibility = System.Windows.Visibility.Collapsed;
            RssDataGrid.Visibility = System.Windows.Visibility.Visible;
            ViewFeedButton.Visibility = System.Windows.Visibility.Collapsed;
            UpdateForm();
        }

        /// <summary>
        /// Updates GUI 
        /// </summary>
        public void UpdateForm()
        {
            
            RssAggregatorServiceClient client = new RssAggregatorServiceClient();
           // client.OpenAsync();
            try
            {
                if (RssDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.GetFeedsCompleted += client_GetFeedsCompleted;
                    client.GetFeedsAsync();

                    ViewFeedButton.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (RssDataGrid.SelectedItem != null && RSSnewsDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.GetNewsCompleted += client_GetNewsCompleted;
                    client.GetNewsAsync(((RSSChannel)RssDataGrid.SelectedItem).ID);

                    ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
                }
                else if (RssDataGrid.SelectedItem == null && RSSnewsDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    client.FetchAllFeedNewsCompleted += client_FetchAllFeedNewsCompleted;
                    client.FetchAllFeedNewsAsync();

                    ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
                }
            }
                
            catch (FaultException<SqlExceptDetails> ex)
            {
                ShowMessage("UpdateForm", ex);
            }
            finally
            {
                client.CloseAsync();
            }
        }

        /// <summary>
        /// Only calls method UpdateForm()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateForm_EventHandler(object sender, EventArgs e)
        {
            UpdateForm();
        }

        /// <summary>
        /// Calls the AddWindow dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFeedButton_Click(object sender, RoutedEventArgs e)
        {           
            var addFeedWindow = new AddWindow(this);
            addFeedWindow.Show();
        }

        /// <summary>
        /// Deletes a feed or news via pushing the "Delete" button on the grid-row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_SingleItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RssDataGrid.Visibility == System.Windows.Visibility.Visible)
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();

                   // client.OpenAsync();
                    client.RemoveFeedCompleted += UpdateForm_EventHandler;
                    client.RemoveFeedAsync(((RSSChannel)RssDataGrid.SelectedItem).ID);
                    client.CloseAsync();
                }
                else
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();

                    //client.OpenAsync();
                    client.RemoveNewsCompleted += UpdateForm_EventHandler;
                    client.RemoveNewsAsync(((RSSNewsItem)RSSnewsDataGrid.SelectedItem).ID);
                    client.CloseAsync();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Delete_SingleItem_Click", ex);
            }
        }

        /// <summary>
        /// Sets the source for RssDataGrid.
        /// e.Result is RssChannel collection type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_GetFeedsCompleted(object sender, GetFeedsCompletedEventArgs e)
        {
            try
            {
                RssDataGrid.ItemsSource = e.Result;
            }
            catch (Exception ex)
            {
                ShowMessage("client_GetFeedsCompleted", ex);
            }
        }

        /// <summary>
        /// Calls the Delete Warning Dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_All_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var warningMessage = new MessageWindow();
                warningMessage.Title = "Warning!";
                warningMessage.MessageTextBlock.Text = "Warning! This action will delete\r\n all saved feeds from database.\r\n Are you sure?";
                warningMessage.Show();
            }
            catch (Exception ex)
            {
                ShowMessage("Delete_All_Click", ex);
            }
        }

        /// <summary>
        /// Sets the source for RSSnewsDataGrid.
        /// e.Result is RssNews collection type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_GetNewsCompleted(object sender, GetNewsCompletedEventArgs e)
        {
            try
            {
                RSSnewsDataGrid.ItemsSource = e.Result;
            }
            catch (Exception ex)
            {
                ShowMessage("client_GetNewsCompleted", ex);
            }
        }

        /// <summary>
        /// Sets the column`s order in RssDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RssDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            try
            {
                switch ((string)e.Column.Header)
                {
                    case "ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;        //hide unusefull columns
                    case "news": e.Column.Visibility = System.Windows.Visibility.Collapsed;
                        RssDataGrid.Columns[0].DisplayIndex = 4; break;

                    case "Name": e.Column.DisplayIndex = 0; break;                                          //
                    case "Title": e.Column.DisplayIndex = 1; e.Column.MaxWidth = 200; break;                 //
                    case "URL": e.Column.DisplayIndex = 2; break;                                          //Set the column`s order in datagrid
                    case "Description": e.Column.DisplayIndex = 3; e.Column.MaxWidth = 600; break;         //
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RssDataGrid_AutoGeneratingColumn", ex);
            }

        }

        /// <summary>
        /// Sets the column`s order in RSSnewsDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RSSnewsDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            try
            {
                switch ((string)e.Column.Header)
                {
                    case "Channel_ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;
                    case "ID": e.Column.Visibility = System.Windows.Visibility.Collapsed; break;
                    case "Title": e.Column.DisplayIndex = 0; RSSnewsDataGrid.Columns[0].DisplayIndex = 3; break;         //
                    case "Description": e.Column.DisplayIndex = 1; e.Column.MaxWidth = 800; break;                     //Set the column`s order in datagrid
                    case "Link": e.Column.DisplayIndex = 2; break;                                                  // 
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RSSnewsDataGrid_AutoGeneratingColumn", ex);
            }
        }

        /// <summary>
        /// Fills RssDataGrid with all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RssDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems.Count != 0 && ((DataGrid)sender).CurrentColumn.Header != null)
                {
                    RssAggregatorServiceClient client = new RssAggregatorServiceClient();
                 //   client.OpenAsync();
                    client.GetNewsCompleted += client_GetNewsCompleted;
                    client.GetNewsAsync(((RSSChannel)e.AddedItems[0]).ID);
                    client.CloseAsync();

                    RssDataGrid.Visibility = System.Windows.Visibility.Collapsed;
                    RSSnewsDataGrid.Visibility = System.Windows.Visibility.Visible;
                    UpdateForm();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RssDataGrid_SelectionChanged", ex);
            }
        }

        /// <summary>
        /// Back to feed view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewFeed_Click(object sender, RoutedEventArgs e)
        {
            RssDataGrid.Visibility = System.Windows.Visibility.Visible;
            RSSnewsDataGrid.Visibility = System.Windows.Visibility.Collapsed;
            UpdateForm();
        }

        /// <summary>
        /// Provides a navigation to URL by click on Link column in RSSnewsDataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RSSnewsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (((DataGrid)sender).CurrentColumn.Header != null && ((DataGrid)sender).CurrentColumn.Header.ToString() == "Link")
                {
                    System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(((RSSNewsItem)((DataGrid)sender).SelectedItem).Link));
                }
            }
            catch (Exception ex)
            {
                ShowMessage("RSSnewsDataGrid_SelectionChanged", ex);
            }
        }

        /// <summary>
        /// Fetchs the news from all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchAllFeed_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RssAggregatorServiceClient client = new RssAggregatorServiceClient();
            //    client.OpenAsync();
                client.FetchAllFeedNewsCompleted += client_FetchAllFeedNewsCompleted;
                client.FetchAllFeedNewsAsync();
                client.CloseAsync();

                RssDataGrid.Visibility = System.Windows.Visibility.Collapsed;
                RSSnewsDataGrid.Visibility = System.Windows.Visibility.Visible;
                ViewFeedButton.Visibility = System.Windows.Visibility.Visible;
            }
            catch (Exception ex)
            {
                ShowMessage("FetchAllFeed_Click", ex);
            }
        }

        /// <summary>
        /// Fills RSSnewsDataGrid with all news from all feeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_FetchAllFeedNewsCompleted(object sender, FetchAllFeedNewsCompletedEventArgs e)
        {
            try { 
            RSSnewsDataGrid.ItemsSource = e.Result;
            }
            catch (Exception ex)
            {
                ShowMessage("client_FetchFeedNewsCompleted", ex);
            }
        }
        /// <summary>
        /// Shows attention window with  exception details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ex"></param>
        private void ShowMessage(string sender, Exception ex)
        {
            MessageWindow exceptionWindow = new MessageWindow();
            exceptionWindow.Title = "Attention! Exception in " + sender;
            exceptionWindow.MessageTextBlock.Text = ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                exceptionWindow.MessageTextBlock.Text += "\r\n" + ex.Message + ex.StackTrace;
            }
            exceptionWindow.Show();
        }
    }
}
