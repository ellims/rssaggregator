﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RSS_Aggregator_Vol4.EllimsRssService;

namespace RSS_Aggregator_Vol4.pages
{
    public partial class MessageWindow : ChildWindow
    {
        public MessageWindow()
        {
            InitializeComponent();
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (this.MessageTextBlock.Text.Contains("delete\r\n all saved feeds"))
            {
                RssAggregatorServiceClient client = new RssAggregatorServiceClient();
                client.OpenAsync();
                client.RemoveAllNewsAsync();
                
                client.CloseAsync();
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

