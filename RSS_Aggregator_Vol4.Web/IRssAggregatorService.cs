﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RSS_Aggregator_Vol4.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRssAggregatorService" in both code and config file together.
    [ServiceContract]
    
    public interface IRssAggregatorService
    {
        [OperationContract]
        [FaultContract(typeof(XmlExceptDetails))]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        string AddFeed(string url, string name);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveFeed(int feed_Id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        List<RSSChannel> GetFeeds();

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        RSSNews FetchAllFeedNews();

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        RSSNews GetNews(int channel_id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveNews(int item_id);

        [OperationContract]
        [FaultContract(typeof(SqlExceptDetails))]
        [FaultContract(typeof(SomeExceptDetails))]
        void RemoveAllNews();
    }
}
