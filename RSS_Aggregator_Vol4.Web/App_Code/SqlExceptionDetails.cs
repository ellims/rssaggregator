﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSS_Aggregator_Vol4.Web
{
    /// <summary>
    /// Stores information about SQL exception throwen on service side 
    /// </summary>
    [DataContract]
    public class SqlExceptDetails
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public string StackTrace { get; set; }

        public SqlExceptDetails() { }
        public SqlExceptDetails(SqlException exception)
        {
            this.Message = exception.Message;
            this.Source = exception.Source;
            this.StackTrace = exception.StackTrace;
        }
    }
}
