﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;


namespace RSS_Aggregator_Vol4.Web
{
    [CollectionDataContract]
    public class RSSNews : List<RSSNewsItem>
    {
        /// <summary>
        /// Check the news for the uniqueness
        /// </summary>
        /// <param name="itemToCheck"></param>
        /// <returns></returns>
   
        /*new*/public bool Contains(XmlNode itemToCheck)
        {
            if (itemToCheck != null)
                foreach (XmlNode innerNode in itemToCheck)
                {
                    if (innerNode.Name == itemToCheck.Name && innerNode.InnerText == itemToCheck.InnerText)
                    {
                        return true;
                    }
                }
            return false;
        }
    }
}