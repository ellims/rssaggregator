﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSS_Aggregator_Vol4.Web
{
    /// <summary>
    /// Stores information about any exception throwen on service side
    /// </summary>
    [DataContract]
    public class SomeExceptDetails
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public string StackTrace { get; set; }

        public SomeExceptDetails() { }
        public SomeExceptDetails(Exception exception)
        {
            this.Message = exception.Message;
            this.Source = exception.Source;
            this.StackTrace = exception.StackTrace;
        }
    }
}
