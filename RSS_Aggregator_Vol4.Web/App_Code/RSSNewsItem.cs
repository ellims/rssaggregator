﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;

namespace RSS_Aggregator_Vol4.Web
{
    /// <summary>
    /// Describes single news in RSS channel
    /// </summary>
    [DataContract]
    public class RSSNewsItem
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Link { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Channel_ID { get; set; }

        public RSSNewsItem() { }
        public RSSNewsItem(XmlNode node)
        {
            if (node != null)
            {
                foreach (XmlNode innerNode in node)
                {
                    switch (innerNode.Name)
                    {
                        case "title": this.Title = innerNode.InnerText; break;
                        case "link": this.Link = innerNode.InnerText; break;
                        case "description": this.Description = innerNode.InnerText; break;
                    }
                }
            }
        }
    }
}