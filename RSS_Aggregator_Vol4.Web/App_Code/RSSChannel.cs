﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;

namespace RSS_Aggregator_Vol4.Web
{
    [DataContract]
    public class RSSChannel
    {
        //Description of the  RSS-Channel
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string URL { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        protected internal int ID { get; set; }
        
        [DataMember]
        protected internal RSSNews news;

        public RSSChannel() { }
        public RSSChannel(string Url)
        {
            news = new RSSNews();
            XmlDocument document = new XmlDocument();
            try
            {
                XmlReaderSettings set = new XmlReaderSettings();
                set.DtdProcessing = DtdProcessing.Parse;
                XmlReader reader = XmlReader.Create(Url,set);
                
                //creating local xml-document with current RSS-channel
                document.Load(reader);
                reader.Close();

                //gathering information of the channel 
                XmlNode channel = document.GetElementsByTagName("channel")[0];
                if (channel != null)
                {
                    foreach (XmlNode innerNode in channel)
                    {
                        switch (innerNode.Name)
                        {                             
                            case "title": this.Title = innerNode.InnerText; break;
                            case "link": this.URL = innerNode.InnerText; break;
                            case "description": this.Description = innerNode.InnerText; break;
                            case "item": RSSNewsItem newsItem = new RSSNewsItem(innerNode);  
                                if(!this.news.Contains(newsItem))
                                    news.Add(newsItem); 
                                break;
                        }
                    }
                }
                else
                    throw new XmlException("XML Error!. Channel description in " + Url + " wasn`t found!");
            }


            catch (System.Net.WebException ex)
            {
                if (ex.Status == System.Net.WebExceptionStatus.NameResolutionFailure)
                    throw new Exception("Can`t connect with source.\r\n" + Url);
                else
                    throw ex;
            }

            catch (System.IO.FileNotFoundException)
            {
                throw new Exception(string.Format("File {0} not found!", Url));
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}