﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Text;
using System.Xml;

namespace RSS_Aggregator_Vol4.Web
{
    /// <summary>
    /// Stores information about XML exception throwen on service side 
    /// </summary>
    [DataContract]
    public class XmlExceptDetails
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public string StackTrace { get; set; }

        public XmlExceptDetails() { }
        public XmlExceptDetails(XmlException exception)
        {
            this.Message = exception.Message;
            this.Source = exception.Source;
            this.StackTrace = exception.StackTrace;
            
        }
    }
}
