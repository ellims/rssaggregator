﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
namespace RSS_Aggregator_Vol4.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RssAggregatorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RssAggregatorService.svc or RssAggregatorService.svc.cs at the Solution Explorer and start debugging.
    public class RssAggregatorService : IRssAggregatorService
    {

       
        /// <summary>
        /// Adds new channel to DataBase 
        /// </summary>
        /// <param name="url">Source URL of added RssChannel</param>
        /// <param name="name">User`s name of added RssChannel</param>
        /// <returns>Returns a string with a number of news in channel</returns>
        public string AddFeed(string url, string name)
        {
            string callbackMessage = "";

            SqlDataReader reader = null;
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);
            try
            {
                RSSChannel currFeed = new RSSChannel(url);
                currFeed.Name = name;
                connection.Open();

                //fill the Channel Table in DataBase
                SqlCommand cmd = new SqlCommand("INSERT INTO channel(name,title,description,url) VALUES(@name,@title,@description,@URL)", connection);
                cmd.Parameters.AddWithValue("@name", currFeed.Name);
                cmd.Parameters.AddWithValue("@title", currFeed.Title);
                cmd.Parameters.AddWithValue("@description", currFeed.Description);
                cmd.Parameters.AddWithValue("@URL", currFeed.URL);

                if (cmd.ExecuteNonQuery() == 1)
                    callbackMessage += "Channel with ";

                //get auto-generated "ID" of current channel for "news" table`s foreign key
                cmd.CommandText = "SELECT TOP 1 id FROM channel ORDER BY id DESC";
                reader = cmd.ExecuteReader();
                reader.Read();
                currFeed.ID = (int)reader["ID"];
                reader.Close();

                //fill the "news" Table in DataBase
                int countOfNews = 0;
                foreach (RSSNewsItem newsItem in currFeed.news)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "INSERT INTO news(title,description,URL,channel_id) VALUES(@title,@description,@URL,@channel_id)";

                    cmd.Parameters.AddWithValue("@title", newsItem.Title);
                    cmd.Parameters.AddWithValue("@description", newsItem.Description);
                    cmd.Parameters.AddWithValue("@URL", newsItem.Link);
                    cmd.Parameters.AddWithValue("@channel_id", currFeed.ID);

                    if (cmd.ExecuteNonQuery() == 1)
                        countOfNews++;

                    cmd.CommandText = "SELECT TOP 1 id FROM news ORDER BY id DESC";
                    reader = cmd.ExecuteReader();
                    reader.Read();
                    newsItem.ID = (int)reader["ID"];
                    reader.Close();
                }
                callbackMessage += countOfNews + " news was added into database.";
                
            }           
            
            catch (XmlException ex)
            {
                XmlExceptDetails details = new XmlExceptDetails(ex);
               // throw new FaultException<XmlExceptDetails>(details, new FaultReason(ex.Message));
                callbackMessage = details.Message;
                return callbackMessage;
            }

            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
               // throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
                callbackMessage = details.Message;
                return callbackMessage;
            }

            catch(Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
               // throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
                callbackMessage = details.Message;
                return callbackMessage;
            }

            finally
            {
                if (reader != null)
                    reader.Close();
                if (connection != null)
                    connection.Close();
            }
            return callbackMessage;
        }
        /// <summary>
        /// Removes channel from DataBase by "Id"     
        /// </summary>
        /// <param name="feed_Id">feed`s ID in database</param>
        public void RemoveFeed(int feed_Id)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);
            try
            {
                connection.Open();
                
                SqlCommand cmd = new SqlCommand("DELETE FROM news WHERE channel_id = " + feed_Id, connection);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DELETE FROM channel WHERE Id = " + feed_Id;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

        /*    catch (Exception ex)
            {
                SomeExceptionDetails details = new SomeExceptionDetails(ex);
                throw new FaultException<SomeExceptionDetails>(details, new FaultReason(ex.Message));
            }*/
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }
        /// <summary>
        /// Gets all channels from database       
        /// </summary>
        /// <returns>the list of channels from database</returns>
        public List<RSSChannel> GetFeeds()
        {
            List<RSSChannel> rssFeedlist = new List<RSSChannel>();
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM channel", connection);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    rssFeedlist.Add(new RSSChannel() 
                                    { Name = (string)reader["name"], 
                                        Description = (string)reader["description"], 
                                        URL = (string)reader["URL"], 
                                        Title = (string)reader["title"],
                                        ID = (int)reader["ID"]});
                }
            }
           catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

           catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (connection != null)
                    connection.Close();
            }

            return rssFeedlist;
        }
        /// <summary>
        /// Returns all news from all feeds
        /// </summary>
        /// <returns></returns>
        public RSSNews FetchAllFeedNews()
        {
            RSSNews rssNewsList = new RSSNews();
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM news", connection);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    RSSNewsItem currentNews = new RSSNewsItem();

                    if (reader["description"] == System.DBNull.Value)
                        currentNews.Description = "";
                    else
                        currentNews.Description = (string)reader["description"];

                    if (reader["URL"] == System.DBNull.Value)
                        currentNews.Link = "";
                    else
                        currentNews.Link = (string)reader["URL"];

                    if (reader["title"] == System.DBNull.Value)
                        currentNews.Title = "";
                    else
                        currentNews.Title = (string)reader["title"];
                    currentNews.ID = (int)reader["ID"];
                    currentNews.Channel_ID = (int)reader["Channel_Id"];

                    rssNewsList.Add(currentNews);
                }
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (connection != null)
                    connection.Close();
            }
            return rssNewsList;
        }
        /// <summary>
        /// Gets news of concrete rssChannel 
        /// </summary>
        /// <param name="channel_id">rssChannel`s Id in database</param>
        /// <returns></returns>
        public RSSNews GetNews(int channel_id)
        {
            RSSNews rssNewsList = new RSSNews();
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);

            SqlDataReader reader = null;
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM news WHERE channel_id = " + channel_id, connection);

                reader = cmd.ExecuteReader();
                

                while (reader.Read())
                {
                    RSSNewsItem currentNews = new RSSNewsItem();

                    if (reader["description"] == System.DBNull.Value)
                        currentNews.Description = "";
                    else
                        currentNews.Description = (string)reader["description"];

                    if (reader["URL"] == System.DBNull.Value)
                        currentNews.Link = "";
                    else
                        currentNews.Link = (string)reader["URL"];

                    if (reader["title"] == System.DBNull.Value)
                        currentNews.Title = "";
                    else
                        currentNews.Title = (string)reader["title"];
                    currentNews.ID = (int)reader["ID"];
                    currentNews.Channel_ID = (int)reader["Channel_Id"];

                    rssNewsList.Add(currentNews);
                }
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (connection != null)
                    connection.Close();
            }
            return rssNewsList;
        }
        /// <summary>
        /// Removes single news by unique ID
        /// </summary>
        /// <param name="item_id">news`s id in database</param>
        public void RemoveNews(int item_id)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);
            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("DELETE FROM news WHERE id = " + item_id, connection);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }
        /// <summary>
        /// Removes all saved feeds from application
        /// </summary>
        public void RemoveAllNews()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["RSS_DB_connectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);
            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand("DELETE FROM news ", connection);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DELETE FROM channel";
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

    }
}
